defmodule Main do
  def main do
    s = IO.gets("") |> String.trim()
    case String.ends_with?(s, "s") do
      true ->
        IO.puts(s <> "es")
      _ ->
        IO.puts(s <> "s")
    end
  end
end
